Categories:System
License:GPL-3.0-only
Author Email:juam+scrambled@posteo.net
Web Site:
Source Code:https://gitlab.com/juanitobananas/scrambled-exif/tree/HEAD
Issue Tracker:https://gitlab.com/juanitobananas/scrambled-exif/issues
Changelog:https://gitlab.com/juanitobananas/scrambled-exif/blob/HEAD/CHANGELOG.md
Bitcoin:16DXeCxxKGvepYLehyHSr3M1nv1s1eUotZ

Auto Name:Scrambled Exif

Repo Type:git
Repo:https://gitlab.com/juanitobananas/scrambled-exif.git

Build:1.0,1
    commit=v1.0
    subdir=app
    gradle=yes

Build:1.0.1b,4
    commit=v1.0.1b
    subdir=app
    gradle=yes

Build:1.0.2,5
    commit=v1.0.2
    subdir=app
    gradle=yes

Build:1.0.3,6
    commit=v1.0.3
    subdir=app
    gradle=yes

Build:1.0.4,7
    commit=v1.0.4
    subdir=app
    gradle=yes

Build:1.1,8
    commit=v1.1
    subdir=app
    gradle=yes

Build:1.1.1,9
    commit=v1.1.1
    subdir=app
    gradle=yes

Build:1.1.2,10
    commit=v1.1.2
    subdir=app
    gradle=yes

Build:1.1.3,11
    commit=v1.1.3
    subdir=app
    submodules=yes
    gradle=yes

Build:1.2,12
    commit=v1.2
    subdir=app
    submodules=yes
    gradle=yes

Build:1.2.1,13
    commit=v1.2.1
    subdir=app
    submodules=yes
    gradle=yes

Build:1.2.2,14
    commit=v1.2.2
    subdir=app
    submodules=yes
    gradle=yes

Build:1.2.4,16
    commit=v1.2.4
    subdir=app
    submodules=yes
    gradle=standard

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.2.4
Current Version Code:16
